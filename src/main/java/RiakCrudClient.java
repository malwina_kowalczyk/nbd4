import com.basho.riak.client.api.RiakClient;
import com.basho.riak.client.api.commands.kv.DeleteValue;
import com.basho.riak.client.api.commands.kv.FetchValue;
import com.basho.riak.client.api.commands.kv.StoreValue;
import com.basho.riak.client.api.commands.kv.UpdateValue;
import com.basho.riak.client.core.RiakCluster;
import com.basho.riak.client.core.RiakNode;
import com.basho.riak.client.core.query.Location;
import com.basho.riak.client.core.query.Namespace;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

class RiakCrudClient<T> {
    private final static Logger LOGGER = Logger.getLogger(RiakCrudClient.class.getName());
    private RiakCluster cluster;
    private RiakClient client = new RiakClient(cluster);


    private static RiakCluster setUpCluster(String address) {
        RiakNode node = new RiakNode.Builder()
                .withRemoteAddress(address)
                .build();

        RiakCluster cluster = new RiakCluster.Builder(node)
                .build();
        cluster.start();

        return cluster;
    }

    RiakCrudClient(String riakAddress) {
        cluster = setUpCluster(riakAddress);
        client = new RiakClient(cluster);
    }

    String insert(String bucketName, String key, T entity) throws ExecutionException, InterruptedException {
        Namespace bucket = new Namespace(bucketName);
        Location storeClientLocation = new Location(bucket, key);
        StoreValue storeEntityOp = new StoreValue.Builder(entity)
                .withLocation(storeClientLocation)
                .build();
        StoreValue.Response response = client.execute(storeEntityOp);
        LOGGER.info(entity.toString()+ " information now stored in Riak");
        return response.getLocation().getKeyAsString();
    }

    String update(String bucketName, String key, UpdateValue.Update<T> entity) throws ExecutionException, InterruptedException {
        Namespace bucket = new Namespace(bucketName);
        Location storeClientLocation = new Location(bucket, key);
        UpdateValue updateValue = new UpdateValue.Builder(storeClientLocation)
                .withUpdate(entity).build();
        UpdateValue.Response response = client.execute(updateValue);

        LOGGER.info(entity.toString()+ " information now stored in Riak");
        return response.getLocation().getKeyAsString();
    }

    void delete(String bucketName, String key) throws ExecutionException, InterruptedException {
        Namespace bucket = new Namespace(bucketName);
        Location location = new Location(bucket, key);
        DeleteValue deleteOp = new DeleteValue.Builder(location)
                .build();
        client.execute(deleteOp);
        LOGGER.info(bucketName + " " + key + " object successfully deleted");
    }

    T fetch(String bucketName, String key, TypeReference<T> typeReference) throws ExecutionException, InterruptedException {
        Namespace bucket = new Namespace(bucketName);
        Location location = new Location(bucket, key);
        FetchValue fetchClientOp = new FetchValue.Builder(location).build();
        T fetchedObject = client.execute(fetchClientOp).getValue(typeReference);
        LOGGER.info(fetchedObject + " object successfully fetched");
        return fetchedObject;
    }

    void shutdown() {
        cluster.shutdown();
    }
}
