import com.basho.riak.client.api.commands.kv.UpdateValue;

public class SessionDetails {

    public String name;
    public boolean underage;
    public String basket;
    public Integer visitCount;

    @Override
    public String toString() {
        return "SessionDetails{" +
                "name='" + name + '\'' +
                ", underage=" + underage +
                ", basket='" + basket + '\'' +
                ", visitCount=" + visitCount +
                '}';
    }

    public static class SessionDetailsUpdate extends UpdateValue.Update<SessionDetails> {

        private final SessionDetails update;

        public SessionDetailsUpdate(SessionDetails update) {
            this.update = update;
        }

        public SessionDetails apply(SessionDetails c) {
            if (c == null) {
                c = new SessionDetails();
            }
            c.name = update.name;
            c.underage = update.underage;
            c.basket = update.basket;
            c.visitCount = update.visitCount;
            return c;
        }
    }
}
