import com.fasterxml.jackson.core.type.TypeReference;

import java.util.UUID;
import java.util.logging.Logger;

public class RiakUsageExample {

    private final static Logger LOGGER = Logger.getLogger(RiakCrudClient.class.getName());
    private static final String BUCKET_NAME = "s19901";
    private static final String LOCALHOST = "127.0.0.1";
    private static  final TypeReference sessionDetailsTypeRef = new TypeReference<SessionDetails>() { };

    public static void main(String[] args) {
        RiakCrudClient crudClient = new RiakCrudClient(LOCALHOST);
        try {

            SessionDetails sessionDetails = new SessionDetails();
            sessionDetails.name = "Kubus Puchatek";
            sessionDetails.underage = true;
            sessionDetails.visitCount = 99;
            sessionDetails.basket = "honey";
            String sessionId = "session_id_" + UUID.randomUUID();

            crudClient.insert(BUCKET_NAME, sessionId, sessionDetails);

            SessionDetails fetchedDetails = (SessionDetails) crudClient.fetch(BUCKET_NAME, sessionId, sessionDetailsTypeRef);
            assert(sessionDetails.getClass() == fetchedDetails.getClass());
            assert(sessionDetails.name.equals(fetchedDetails.name));
            assert(sessionDetails.basket.equals(fetchedDetails.basket));
            assert(sessionDetails.underage == fetchedDetails.underage);
            assert(sessionDetails.visitCount.equals(fetchedDetails.visitCount));

            sessionDetails.visitCount++;
            crudClient.update(BUCKET_NAME, sessionId, new SessionDetails.SessionDetailsUpdate(sessionDetails));


            fetchedDetails = (SessionDetails) crudClient.fetch(BUCKET_NAME, sessionId, sessionDetailsTypeRef);
            assert(fetchedDetails.visitCount.equals(100));

            crudClient.delete(BUCKET_NAME, sessionId);

            fetchedDetails = (SessionDetails) crudClient.fetch(BUCKET_NAME, sessionId, sessionDetailsTypeRef);
            assert (fetchedDetails == null);

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            crudClient.shutdown();
        }
    }
}
