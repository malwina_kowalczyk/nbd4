### Zadanie 1
#####Query

```
curl -XPUT "http://localhost:8098/buckets/s19901/keys/session_id_1" -i -H "Content-Type: application/json" -d '{"name": "Alice K", "visit_count": 3, "basket": ["coke" ,"bread"], "underage": false}'
curl -XPUT "http://localhost:8098/buckets/s19901/keys/session_id_2" -i -H "Content-Type: application/json" -d '{"name": "Zibi Dibi", "visit_count": 1, "basket": ["beer"], "underage": true}'
curl -XPUT "http://localhost:8098/buckets/s19901/keys/session_id_3" -i -H "Content-Type: application/json" -d '{"name": "Malwina Kowalczyk", "visit_count": 10, "basket": ["cotton pads"], "underage": false}'
curl -XPUT "http://localhost:8098/buckets/s19901/keys/session_id_4" -i -H "Content-Type: application/json" -d '{"name": "Franek Kimono", "visit_count": 4, "basket": ["cheese", "chicken"], "underage": false}'
curl -XPUT "http://localhost:8098/buckets/s19901/keys/session_id_5" -i -H "Content-Type: application/json" -d '{"name": "Sponge Bob", "visit_count": 3, "basket": ["sugar"], "underage": true}'
```
#####Response

```
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Tue, 04 Jun 2019 22:59:14 GMT
Content-Type: application/json
Content-Length: 0

HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Tue, 04 Jun 2019 22:59:30 GMT
Content-Type: application/json
Content-Length: 0

HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Tue, 04 Jun 2019 22:59:34 GMT
Content-Type: application/json
Content-Length: 0

HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Tue, 04 Jun 2019 22:59:38 GMT
Content-Type: application/json
Content-Length: 0

HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Tue, 04 Jun 2019 22:59:44 GMT
Content-Type: application/json
Content-Length: 0
```
___
### Zadanie 2
Pobranie rekodu
#####Query
```
curl http://localhost:8098/buckets/s19901/keys/session_id_3 -i
```
#####Response
```
HTTP/1.1 200 OK
X-Riak-Vclock: a85hYGBgzGDKBVI8ypz/fu5k62BjYPBTzGBKZMxjZbhWnH2NLwsA
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Link: </buckets/s19901>; rel="up"
Last-Modified: Tue, 04 Jun 2019 22:59:34 GMT
ETag: "1FDG1oXaKVh2N9ayd5lEXn"
Date: Tue, 04 Jun 2019 23:01:34 GMT
Content-Type: application/json
Content-Length: 94
{"name": "Malwina Kowalczyk", "visit_count": 10, "basket": ["cotton pads"], "underage": false}
```
### Zadanie 3
#####Query
Dodanie pola `transaction_started`
```
curl -XPUT -i -H "Content-Type: application/json" -d '{"name": "Malwina Kowalczyk", "visit_count": 10, "basket": ["cotton pads"], transaction_started: true}' http://localhost:8098/buckets/s19901/keys/session_id_3
curl http://localhost:8098/buckets/s19901/keys/session_id_3 -i
```
#####Response
```
HTTP/1.1 200 OK
X-Riak-Vclock: a85hYGBgzGDKBVI8ypz/fu5k62BjYPBTzGBKZMpjZQgvzb7GlwUA
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Link: </buckets/s19901>; rel="up"
Last-Modified: Tue, 04 Jun 2019 23:05:59 GMT
ETag: "1E896AeHVRCiZkiVVtL4rb"
Date: Tue, 04 Jun 2019 23:06:22 GMT
Content-Type: application/json
Content-Length: 121
{"name": "Malwina Kowalczyk", "visit_count": 10, "basket": ["cotton pads"], "underage": false, transaction_started: true}
```

### Zadanie 4
Usunięcie pola `underage`
####Query
```
curl -XPUT -i -H "Content-Type: application/json" -d '{"name": "Malwina Kowalczyk", "visit_count": 10, "basket": ["cotton pads"], transaction_started: true}' http://localhost:8098/buckets/s19901/keys/session_id_3
curl http://localhost:8098/buckets/s19901/keys/session_id_3 -i
```
#####Response
```
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Tue, 04 Jun 2019 23:07:20 GMT
Content-Type: application/json
Content-Length: 0
___
{"name": "Malwina Kowalczyk", "visit_count": 10, "basket": ["cotton pads"], transaction_started: true}

```
### Zadanie 5
Zwiększenie licznika `visit_count` i listy `basket`
#####Query
```
curl -XPUT -i -H "Content-Type: application/json" -d '{"name": "Malwina Kowalczyk", "visit_count": 11, "basket": ["cotton pads", "chocolate"], "underage": false, transaction_started: true}' http://localhost:8098/buckets/s19901/keys/session_id_3
curl http://localhost:8098/buckets/s19901/keys/session_id_3 -i
```
#####Response
```
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Tue, 04 Jun 2019 23:08:46 GMT
Content-Type: application/json
Content-Length: 0
___
HTTP/1.1 200 OK
X-Riak-Vclock: a85hYGBgzGDKBVI8ypz/fu5k62BjYPBTzGBKZMljZfhXmn2NLwsA
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Link: </buckets/s19901>; rel="up"
Last-Modified: Tue, 04 Jun 2019 23:08:46 GMT
ETag: "yCctsoDbPIbz71Rwm5ka"
Date: Tue, 04 Jun 2019 23:08:50 GMT
Content-Type: application/json
Content-Length: 134

{"name": "Malwina Kowalczyk", "visit_count": 11, "basket": ["cotton pads", "chocolate"], "underage": false, transaction_started: true}

```
### Zadanie 6
Usunięcie rekordu `session_id_3`
####Query
```
curl -XDELETE http://localhost:8098/buckets/s19901/keys/session_id_3 -i
```
#####Response
```
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Tue, 04 Jun 2019 23:10:38 GMT
Content-Type: application/json
Content-Length: 0
```
### Zadanie 7
Pobranie rekordu `session_id_3`
####Query
```
curl http://localhost:8098/buckets/s19901/keys/session_id_3 -i
```
#####Response
```
HTTP/1.1 404 Object Not Found
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Tue, 04 Jun 2019 23:12:38 GMT
Content-Type: text/plain
Content-Length: 10
```

### Zadanie 8
####Query
```
curl -XPOST "http://localhost:8098/buckets/s19901/keys" -i -H "Content-Type: application/json" -d '{"name": "Malwina Kowalczyk"}'
```
#####Response
```
HTTP/1.1 201 Created
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Location: /buckets/s19901/keys/UrSkQxqqbFUgKpUcEPEL62JSLEx
Date: Wed, 05 Jun 2019 12:06:57 GMT
Content-Type: application/json
Content-Length: 0
```

### Zadanie 9
####Query
```
curl http://localhost:8098/buckets/s19901/keys/UrSkQxqqbFUgKpUcEPEL62JSLEx -i
```
#####Response
```
HTTP/1.1 200 OK
X-Riak-Vclock: a85hYGBgzGDKBVI8ypz/fu74rVzBwOCnlMGUyJjHypCok3ONLwsA
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Link: </buckets/s19901>; rel="up"
Last-Modified: Wed, 05 Jun 2019 12:06:57 GMT
ETag: "4sgV3ap8dL6Ml9URfvhw0L"
Date: Wed, 05 Jun 2019 12:07:45 GMT
Content-Type: application/json
Content-Length: 29

{"name": "Malwina Kowalczyk"}
```
### Zadanie 10
####Query
```
curl -XDELETE http://localhost:8098/buckets/s19901/keys/UrSkQxqqbFUgKpUcEPEL62JSLEx -i
```
#####Response
```
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Wed, 05 Jun 2019 12:09:02 GMT
Content-Type: application/json
Content-Length: 0
```